import { configureStore } from "@reduxjs/toolkit";
import auth from './auth';
import promoDiscount from "./promoDiscount";
import promoDiscountEdit from "./promoDiscountEdit";
import promoFreeGoods from "./promoFreeGoods";
import promoFreeGoodsEdit from "./promoFreeGoodsEdit";
import promotion from './promotion';
import promotionData from "./promotionData";
import promotionItem from "./promotionItem";
import promotionItemEdit from "./promotionItemEdit";
import searchDocument from "./searchDocument";
import transaction from "./transaction";

const store = configureStore({
    reducer: {
        auth: auth,
        promotion: promotion,
        searchDocument: searchDocument,
        promotionData: promotionData,
        promoItem:promotionItem,
        promoDiscount:promoDiscount,
        promoFreeGoods:promoFreeGoods,
        promoDiscountEdit:promoDiscountEdit,
        promoFreeGoodsEdit:promoFreeGoodsEdit,
        promoItemEdit:promotionItemEdit,
        transaction: transaction
    }
});

export default store;