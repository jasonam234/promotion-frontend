import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    transaction : [
        {
            "description": "",
            "invoice_number": "",
            "transaction_id": 0,
            "total_price": 0,
            "transaction_date": ""
        }
    ]
};

const transactionSlice = createSlice({
    name:"transaction",
    initialState:initialState,
    reducers:{
        allTransaction(state,data){
            console.log(data.payload)
            state.transaction=data.payload;
        }
    }
})

export const transactionActions = transactionSlice.actions;
export default transactionSlice.reducer;