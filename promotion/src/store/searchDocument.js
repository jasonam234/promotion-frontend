import { createSlice } from "@reduxjs/toolkit";

const initialStateDoc = {
    search : [
        {
            promotion_id: 0,
            document_number: ""
        }
    ]
};

const searchDocumentSlice = createSlice({
    name:"search_document",
    initialState : initialStateDoc,
    reducers: {
        allDocumentNumber(state, data){
            state.search = data.payload;
        }
    }
})

export const documentActions = searchDocumentSlice.actions;
export default searchDocumentSlice.reducer;