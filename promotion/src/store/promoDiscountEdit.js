import { createSlice } from "@reduxjs/toolkit";

const initialStateDiscount={
    discount:
        {
            "discount_value": 0
        }
};

const discountEditSlice=createSlice({
    name:"discount_edit_data",
    initialState:initialStateDiscount,
    reducers:{
        discountEditData(state,data){
            state.discount=data.payload;
        }
    }
})

export const discountEditActions=discountEditSlice.actions;
export default discountEditSlice.reducer;