import { createSlice } from "@reduxjs/toolkit";

const initialStatePromotion = {
    promotion : {
        description: "",
        document_number: "",
        end_date: "",
        promo_value: "",
        promotion_id: 0,
        start_date: "",
        last_edit: ""
    }
};

const promotionDataSlice = createSlice({
    name:"promotion_data",
    initialState : initialStatePromotion,
    reducers: {
        promotionData(state,data){
            state.promotion = data.payload;
        }
    }
})

export const promotionDataActions = promotionDataSlice.actions;
export default promotionDataSlice.reducer;