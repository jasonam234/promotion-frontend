import { createSlice } from "@reduxjs/toolkit";

const initialStatePromoItem = {
    item:[
        {
          "name": "",
          "product_id": 0,
          "product_code": ""
        },
        {
          "name": "",
          "product_id": 0,
          "product_code": ""
        },
        {
          "name": "",
          "product_id": 0,
          "product_code": ""
        }
    ]
};

const promoItemEditSlice = createSlice({
    name:"promotion_item",
    initialState:initialStatePromoItem,
    reducers:{
        promoItemEdit(state,data){
            state.item=data.payload
        }
    }
})

export const promotionItemEditActions=promoItemEditSlice.actions;
export default promoItemEditSlice.reducer;