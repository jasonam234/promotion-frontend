import { createSlice } from "@reduxjs/toolkit";

const initialStateDiscount={
    discount:
        {
            "discount_value": 0
        }
    
};

const discountSlice=createSlice({
    name:"discount_data",
    initialState:initialStateDiscount,
    reducers:{
        discountData(state,data){
            state.discount=data.payload;
        }
    }
})

export const discountActions=discountSlice.actions;
export default discountSlice.reducer;