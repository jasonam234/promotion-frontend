import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    promotion : [
        {
            description: "",
            document_number: "",
            end_date: "",
            last_edit: "",
            promo_value: "",
            promotion_id: 1,
            start_date: "",
        }
    ]
};

const promotionSlice = createSlice({
    name:"promotion",
    initialState : initialState,
    reducers: {
        allPromotion(state, data){
            state.promotion = data.payload;
        }
    }
})

export const promotionActions = promotionSlice.actions;
export default promotionSlice.reducer;