import { createSlice } from "@reduxjs/toolkit";

const initialStatePromoItem = {
    item:[
        {
          "name": "",
          "product_id": 0,
          "product_code": ""
        }
    ]
};

const promoItemSlice = createSlice({
    name:"promotion_item",
    initialState:initialStatePromoItem,
    reducers:{
        promoItem(state,data){
            state.item=data.payload
        }
    }
})

export const promotionItemActions=promoItemSlice.actions;
export default promoItemSlice.reducer;