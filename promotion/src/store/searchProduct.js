import { createSlice } from "@reduxjs/toolkit";

const initialStateProduct = {
    search : [
        {
            product_id: ""
        }
    ]
};

const searchProductIdSlice = createSlice({
    name:"search_product_id",
    initialState : initialStateProduct,
    reducers: {
        allProductId(state, data){
            state.search = data.payload;
        }
    }
})

export const productIdActions = searchProductIdSlice.actions;
export default searchProductIdSlice.reducer;