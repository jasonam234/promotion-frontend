import { createSlice } from "@reduxjs/toolkit";
import jwt_decode from 'jwt-decode';

const initialState = {
    isAuth:false,
    userDetail: {},
    token: null,
    isError : null,
    isVerify:false
}

const authSlice = createSlice({
    name : "auth",
    initialState : initialState,
    reducers: {
        login(state, data){
            localStorage.setItem("token", data.payload);
            let userData = jwt_decode(data.payload);
            localStorage.setItem("user_id", userData.user_id);
            localStorage.setItem("exp", userData.exp);
            state.userDetail = userData;
            state.token = data.payload
            if(localStorage.getItem("token").length > 0){
                state.isAuth = true;
            }else{
                state.isAuth = false;
            }
        },
        logout(state) {
            localStorage.clear();
            state.isAuth = false;
        },
    }
})

export const authActions = authSlice.actions;
export default authSlice.reducer;