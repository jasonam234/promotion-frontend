import { createSlice } from "@reduxjs/toolkit";

const initialStateFreeGoods={
    freeGoods:
        {
          "name": "",
          "product_id": 0,
          "product_code": "",
          "quantity": 0
        }
    
};

const freeGoodsSlice=createSlice({
    name:"free_goods",
    initialState:initialStateFreeGoods,
    reducers:{
        freeGoodsData(state,data){
            state.freeGoods=data.payload;
        }
    }
})

export const freeGoodsActions=freeGoodsSlice.actions;
export default freeGoodsSlice.reducer;