import { createSlice } from "@reduxjs/toolkit";

const initialStateFreeGoods={
    freeGoods:
        {
          "name": "",
          "product_id": 0,
          "product_code": "",
          "quantity": 0
        }
    
};

const freeGoodsEditSlice=createSlice({
    name:"free_goods",
    initialState:initialStateFreeGoods,
    reducers:{
        freeGoodsEditData(state,data){
            state.freeGoods=data.payload;
        }
    }
})

export const freeGoodsEditActions=freeGoodsEditSlice.actions;
export default freeGoodsEditSlice.reducer;