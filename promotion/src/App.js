import { useSelector } from 'react-redux';
import { Route, Routes, useLocation } from "react-router-dom";
import { Fragment } from 'react';
import { useNavigate } from "react-router";
import { useEffect } from "react";

import Login from './screens/login/Login';
import Register from './screens/register/Register';
import Promotion from './screens/promotion/Promotion';
import NotFound from './screens/not_found/NotFoundPage'

import './App.css';
import NavBarComponent from './components/navbar/NavBarComponent';
import AddPromotion from './screens/promotion/addForm/AddPromotion';
import DetailPromotion from './screens/promotion/detailPromotion/DetailPromotion';
import EditPromotion from './screens/promotion/editPromotion/EditPromotion';
import Transaction from './screens/transaction/Transaction';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  let navigate = useNavigate();
  const location = useLocation();
  const [, checkLocation] = location.pathname.split("/");

  const isAuth = useSelector((state) => {
    return state.auth.isAuth;
  })

  const token = localStorage.getItem("token");
  //clear session
  const exp = localStorage.getItem("exp");
  const expiryDate = new Date(exp * 1000);
  const now = Date.now();
  const dates = new Date(now);

  const sessionCheck = () => {
    if (token && dates >= expiryDate) {
        localStorage.clear();
        window.location.reload();
        navigate("/login");
    }
  };

  const authCheck = async () => {
    if (token) {
      <Route path="/" element={<Login/>} />
    }else if (!token) {
      <Route path="/promotion" element={<Promotion/>} />
    }
  };

  useEffect(() => {
    const getAuthCheck = async () => {
        await authCheck();
        sessionCheck();
    };
    getAuthCheck();
  }, [isAuth]);

  return (
      <Fragment>
        {isAuth || checkLocation === "promotion" || checkLocation === "transaction" ? (
        <>
          <NavBarComponent/>
          <Routes>
            <Route path="/promotion" element={<Promotion/>} />
            <Route path="/promotion/add_promotion" element={<AddPromotion/>} />
            <Route path="/promotion/promotion_detail/:promotion_id" element={<DetailPromotion/>} />
            <Route path="/promotion/promotion_edit/:promotion_id" element={<EditPromotion/>} />
            <Route path="/transaction" element={<Transaction/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </>
      ) : (
        <>
        <Routes>
          <Route path="/" element={<Login/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="*" element={<NotFound/>} />
        </Routes>
        </>
      )}
      </Fragment>
  );
}

export default App;
