import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import ReCAPTCHA from "react-google-recaptcha";
import FormComponent from '../../components/form/FormComponent';
import ButtonComponent from '../../components/button/ButtonComponent';
import TextComponent from '../../components/text/TextComponent';
import Swal from 'sweetalert2';

import { authActions } from '../../store/auth';
import { register } from '../../api/EndPointAuth';
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from 'react';
import { RECAPTCHA_KEY } from "../../constant/constant";
import { getAllDocument, getAllProductId } from "../../api/EndPointPromotion";
import { documentActions } from '../../store/searchDocument';

const FormRegister = () => {
    let navigate = useNavigate();
    let dispatch = useDispatch();

    let [data, setData] = useState({
        username:"",
        password:""
    });

    const [isVerify, setVerify] = useState(false);

    const verifyHandler = () => {
      setVerify(true);
    };

    function usernameHandler(events){
        setData({
            ...data, username: events.target.value
        });
    }

    function passwordHandler(events){
        setData({
            ...data, password: events.target.value
        });
    }

    function registerHandler(events){
        events.preventDefault();
        if (data.username === "" || data.password === "") {
            Swal.fire({
                icon: 'error',
                text: 'Username or Password Must be Filled !'
              })
            return;
        }else {
            register(data)
            .then((res) => {
                Swal.fire({
                    icon: 'success',
                    text: 'Registered !'
                  })
                console.log(res);
                navigate("/");
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    text: 'Username Already Exist !'
                  })
                navigate("/register");
            });
        }
    }

    return(
        <Container>
            <Row>
                <Col>
                    <div>
                        <TextComponent className="color-text" content="Username"/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <FormComponent className="mb-3" onChange= { usernameHandler } 
                        controlId="usernameInput" type="text"  
                        placeholder="username"/>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div>
                        <TextComponent className="color-text" content="Password"/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form>
                        <FormComponent className="mb-3" onChange= { passwordHandler } 
                            controlId="passwordInput" type="password"
                            placeholder="password"/>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <ReCAPTCHA
                    sitekey={RECAPTCHA_KEY}
                    onChange={verifyHandler}
                    />
                </Col>
            </Row>
            <Row className="mt-3">
                <Col>
                    <ButtonComponent className={ isVerify ? "custom-button-active" : "custom-button-inactive" } 
                        onClick={ registerHandler } disabled={ !isVerify } content="Register" />
                </Col>
            </Row>
            <Row>
                <Col className="mt-1">
                    <Link to="/">
                        <TextComponent className="x-small" content="Back to Login"/>
                    </Link>
                </Col>
            </Row>
            
        </Container>
    );

}

export default FormRegister;