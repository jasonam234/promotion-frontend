import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import TextComponent from '../../components/text/TextComponent';
import FormRegister from './FormRegister';

import { Link, useLocation } from "react-router-dom";

import './Login.css';

const Register = () => {
    let location = useLocation();

    return(
        <div>
            <Container>
                <Row>
                    <Col>
                        <div className="cards">
                            <Card>
                                <center>
                                    <Row>
                                        <Col>
                                            <TextComponent className = "title-active" content ="OptiPOS"></TextComponent>
                                        </Col>
                                    </Row>
                                    <hr/>
                                </center>
                                <Card.Body>
                                    <Container>
                                        <FormRegister/>
                                    </Container>
                                </Card.Body>
                            </Card>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Register;