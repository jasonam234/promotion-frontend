import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import ReCAPTCHA from "react-google-recaptcha";
import FormComponent from '../../components/form/FormComponent';
import ButtonComponent from '../../components/button/ButtonComponent';
import TextComponent from '../../components/text/TextComponent';
import Swal from 'sweetalert2';

import { authActions } from '../../store/auth';
import { login } from '../../api/EndPointAuth';
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useState } from 'react';
import { RECAPTCHA_KEY } from "../../constant/constant";

const FormLogin = () => {
    let navigate = useNavigate();
    let dispatch = useDispatch();

    let [data, setData] = useState({
        username:"",
        password:""
    });

    const [isVerify, setVerify] = useState(false);

    const verifyHandler = () => {
      setVerify(true);
    };

    function usernameHandler(events){
        setData({
            ...data, username: events.target.value
        });
    }

    function passwordHandler(events){
        setData({
            ...data, password: events.target.value
        });
    }

    const loginHandler = async (events) => {
        events.preventDefault();
        if (data.username === "" || data.password === "") {
            Swal.fire({
                icon: 'error',
                text: 'Username or Password Must be Filled'
            })
            return;
        }else{
            login(data).then((response) => {
                dispatch(authActions.login(response.data.data));
                navigate('/promotion');
            }).catch(() => {
                Swal.fire({
                    icon: 'error',
                    text: 'Wrong Username or Password'
                })
            });
        }
    };

    return(
        <Container>
            <Row>
                <Col>
                    <div>
                        <TextComponent className="color-text" content="Username"/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <FormComponent className="mb-3" onChange= { usernameHandler } 
                        controlId="usernameInput" type="text"  
                        placeholder="username"/>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div>
                        <TextComponent className="color-text" content="Password"/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form>
                        <FormComponent className="mb-3" onChange= { passwordHandler } 
                            controlId="passwordInput" type="password"
                            placeholder="password"/>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <ReCAPTCHA
                    sitekey={RECAPTCHA_KEY}
                    onChange={verifyHandler}
                    />
                </Col>
            </Row>
            <Row className="mt-3">
                <Col>
                    <ButtonComponent className={ isVerify ? "custom-button-active" : "custom-button-inactive" } 
                        onClick={ loginHandler } disabled={ !isVerify } content="Login" />
                </Col>
            </Row>
            <Row>
                <Col className="mt-1">
                    <Link to="/register">
                        <TextComponent className="x-small" content="Register Here"/>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}

export default FormLogin;