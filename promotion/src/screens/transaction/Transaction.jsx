import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllTransaction } from "../../api/EndPointTransaction";
import { transactionActions } from "../../store/transaction";
import TransactionSearchForm from "./TransactionSearchForm";

import TransactionTable from "./TransactionTable";

import './Transaction.css';

const Transaction = () => {
    let [isLoading, setIsLoading] = useState(false);
    let dispatch = useDispatch();

    useEffect(() => {
        setIsLoading(true);
        getAllTransaction().then((response) => {
            console.log(response.data.data)
            dispatch(transactionActions.allTransaction(response.data.data));
            setIsLoading(false);
        })        
    }, [dispatch])

    if(isLoading === true){
        return(
            <p>Loading . . .</p>
        );
    }

    return(
        <div className="mt-3 mx-auto px-2">
            <TransactionSearchForm/>
            <TransactionTable/>
        </div>
    );
}

export default Transaction;