import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider, PaginationListStandalone } from 'react-bootstrap-table2-paginator';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

import { useSelector, useDispatch } from "react-redux";
import { FaTrash, FaPen, FaEye } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { generateInvoicePdf } from '../../api/EndPointTransaction';

import './TransactionTable.css'

const TransactionTable = () => {
    let dispatch = useDispatch();

    let transactions = useSelector((state) => {
        return state.transaction.transaction;
    })

    function priceFormatter(cell) {      
        return (
            <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(cell)}</span>
        );
    }

    const options = {
        sizePerPage: 10,
        hideSizePerPage: true,
        hidePageListOnlyOnePage: true
    };

    function generateInvoice(transaction_id){
        generateInvoicePdf(transaction_id).then((response) => {
            if(response.status == 200){
                window.open("http://127.0.0.1:8081/api/transaction/get/invoice/" + transaction_id);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error Generating Invoice !',
                    confirmButtonText: 'OK'
                })
            }
        })
    }

    function buttonFormatter(cell){
        return(
            <div>
                <Button variant="primary" className="custom-table-button" size="sm" onClick={ () => generateInvoice(cell) }>
                    <FaEye className="custom-icon-promotion" size="8px" />
                </Button>
            </div>

        );
    }

    const columns = [
        { dataField: 'transaction_id',
        text: 'Transaction ID',
        sort: true,
        hidden: true
        },
        { dataField: 'invoice_number',
        text: 'Invoice Number'
        },
        { dataField: 'description',
        text: 'Description'
        },
        { dataField: 'total_price',
        formatter: priceFormatter,
        headerAlign: 'right',
        align: 'right',
        text: 'Total Price'
        },
        { dataField: 'transaction_date',
        headerAlign: 'right',
        align: 'right',
        text: 'Transaction Date'
        }, { dataField: 'transaction_id',
        formatter: buttonFormatter,
        text: 'Action'
        }];

    const defaultSorted = [{
        dataField: 'transaction_id',
        order: 'desc'
    }];

    return(
        <div className="scrollTable">
            <PaginationProvider
            pagination={ paginationFactory(options) }
            >
            {
                ({
                paginationProps,
                paginationTableProps
                }) => (
                <div>
                    <BootstrapTable
                    keyField="id"
                    data={ transactions }
                    columns={ columns }
                    { ...paginationTableProps }
                    defaultSorted={ defaultSorted } 
                    />                    
                </div>
                )
            }
            </PaginationProvider>
        </div>
    );
}

export default TransactionTable;