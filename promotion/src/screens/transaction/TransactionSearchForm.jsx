import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IoRefreshCircle } from "react-icons/io5";
import { Link, useLocation } from "react-router-dom";

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import FormComponent from '../../components/form/FormComponent';
import ButtonComponent from '../../components/button/ButtonComponent';
import TextComponent from '../../components/text/TextComponent';
import Button from 'react-bootstrap/Button';
import { postTransactionSearch } from "../../api/EndPointTransaction";
import { transactionActions } from "../../store/transaction";

const TransactionSearchForm = () => {
    let dispatch = useDispatch();

    useEffect(() => {
        postTransactionSearch(data).then((response) => {
            dispatch(transactionActions.allTransaction(response.data.data))
        })
    });

    let [data, setData] = useState({
        invoice_number:""
    });

    function invoiceHandler(events){
        setData((prevValue) => {
            return{
                ...prevValue,
                invoice_number: events.target.value
            }
        })
    }

    function refreshHandler(){
        postTransactionSearch(data).then((response) => {
            dispatch(transactionActions.allTransaction(response.data.data))
        })
    }

    return(
        <Row>
            <Col xs={1}>
                <TextComponent className="promotion-search" content ="Search"></TextComponent>
            </Col>
            <Col xs={3}>
                <FormComponent className="mb-3"
                    controlId="invoiceNumber" type="text"  
                    placeholder="invoice number" onChange={ invoiceHandler }/>
            </Col>
            <Col className="mx-2">
                <Button className="custom-add-button mx-2" size="sm" onClick={ () => refreshHandler() }><IoRefreshCircle size="18px" /></Button>
            </Col>
        </Row>
    );
}

export default TransactionSearchForm;