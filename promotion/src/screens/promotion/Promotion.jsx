import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import PromotionTable from "./PromotionTable";
import SearchForm from "./SearchForm";

const Promotion = () => {
    let [isLoading, setIsLoading] = useState(false);

    if(isLoading === true){
        return(
            <p>Loading . . .</p>
        );
    }

    return(
        <div className="mt-3 mx-auto px-2">
            <SearchForm/>    
            <PromotionTable/>
        </div>
    );
}

export default Promotion;