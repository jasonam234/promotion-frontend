import { useState, useEffect } from "react";
import { postPromoSearch } from '../../api/EndPointPromotion';
import { useDispatch, useSelector } from "react-redux";
import { promotionActions } from "../../store/promotion";
import { FaPlusCircle } from "react-icons/fa";
import { IoRefreshCircle } from "react-icons/io5";
import { Link, useLocation } from "react-router-dom";

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import FormComponent from '../../components/form/FormComponent';
import ButtonComponent from '../../components/button/ButtonComponent';
import TextComponent from '../../components/text/TextComponent';
import Button from 'react-bootstrap/Button';

import './SearchForm.css';

const SearchForm = () => {
    let dispatch = useDispatch();

    let documents = useSelector((state) => {
        return state.searchDocument.search;
    })

    useEffect(() => {
        console.log(data);
        postPromoSearch(data).then((response) => {
            console.log(response.data.data)
            dispatch(promotionActions.allPromotion(response.data.data))
        })
    });

    let [data, setData] = useState({
        product_id:"",
        document_number:""
    });

    function productHandler(events){
        setData((prevValue) => {
            return{
                ...prevValue,
                product_id: events.target.value
            }
        })
    }

    function documentHandler(events){
        setData((prevValue) => {
            return{
                ...prevValue,
                document_number: events.target.value
            }
        })
    }

    function refreshHandler(){
        postPromoSearch(data).then((response) => {
            dispatch(promotionActions.allPromotion(response.data.data))
        })
    }

    return(
        <Row>
            <Col xs={1}>
                <TextComponent className="promotion-search" content ="Search"></TextComponent>
            </Col>
            <Col xs={3}>
                <FormComponent className="mb-3" 
                    controlId="productId" type="text"  
                    placeholder="product ID" 
                    onChange={ productHandler }/>
            </Col>
            <Col xs={3}>
                <FormComponent className="mb-3"
                    controlId="documentNumber" type="text"  
                    placeholder="document number" onChange={ documentHandler }/>
            </Col>
                
            <Col className="mx-2">
                <Link to ="/promotion/add_promotion">
                    <Button className="custom-add-button" size="sm"><FaPlusCircle size="18px" /></Button>
                </Link>
                <Button className="custom-add-button mx-2" size="sm" onClick={ () => refreshHandler() }><IoRefreshCircle size="18px" /></Button>
            </Col>
        </Row>
    );
}

export default SearchForm;