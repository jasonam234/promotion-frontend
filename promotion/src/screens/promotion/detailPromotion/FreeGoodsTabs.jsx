import { FaSearch } from "react-icons/fa";
import { useSelector } from 'react-redux';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';

const FreeGoodsTabs = () => {
    const promoFreeGoods=useSelector((state) => {
        return state.promoFreeGoods.freeGoods;
    })
    console.log(promoFreeGoods);

    return(
        <Tabs defaultActiveKey="free-goods" id="uncontrolled-tab-example" >
            <Tab eventKey="free-goods" title="Free Goods" className="mt-1">
                <Row>
                    <Col>
                        <Row>
                            <Col md={3}>
                                <TextComponent className="add-promotion-text" content ="Product Name" />
                            </Col>
                            <Col md={3}>
                                <TextComponent className="add-promotion-text" content ="Item Quantity" />
                            </Col>
                            <Col md={3}>
                                <TextComponent className="add-promotion-text" content ="Product Code" />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={3}>
                                <FormComponent className="mb-3" 
                                controlId="promoItem" type="text"  
                                placeholder={ promoFreeGoods.name }
                                readOnly="yes" />
                            </Col>
                            <Col md={3}>
                                <FormComponent className="mb-3" 
                                controlId="promoItem" type="number"  
                                placeholder={ promoFreeGoods.quantity }
                                readOnly="yes"/>
                            </Col>
                            <Col md={3}>
                                <FormComponent className="mb-3" 
                                controlId="promoItem" type="text"  
                                placeholder={ promoFreeGoods.product_code }
                                readOnly="yes"/>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ButtonComponent className="custom-button-active" content="Submit"/>
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    );
}

export default FreeGoodsTabs;