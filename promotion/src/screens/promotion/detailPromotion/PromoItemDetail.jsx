import { FaSearch } from "react-icons/fa";
import { useEffect, useState } from 'react';
import { getItemCodeSearch } from '../../../api/EndPointPromotion';
import { useSelector } from 'react-redux';

import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

import './PromoItemDetail.css'

const PromoItemDetail = () => {
    const promoItem = useSelector((state) => {
        return state.promoItem.item;
    })

    return(
        <Card className="item-card">
            {promoItem.map((value, index) => {
                let content = `Promo Item #${index+1}`
                return(
                    <Row className="mx-1 my-1" key={index}>
                        <Col>
                            <Row>
                                <Col sm={5}>
                                    <TextComponent className="add-promotion-text" content={content} />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={5}>
                                    <FormComponent className="mb-3" 
                                    controlId="promoItem" type="text"  
                                    placeholder={ value.name }
                                    readOnly="yes" />
                                </Col>
                                <Col sm={5}>
                                    <FormComponent className="mb-3" 
                                    controlId="promoItem" type="text"  
                                    placeholder={ value.product_code }
                                    readOnly="yes"/>
                                </Col>

                            </Row>
                        </Col>
                    </Row>
                );
            })}
            
        </Card>
    );
}

export default PromoItemDetail;