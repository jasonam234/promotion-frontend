import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import PromoDataDetail from "./PromoDataDetail";
import PromoItemDetail from './PromoItemDetail';
import PromoTypeDetail from './PromoTypeDetail';

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPromotionByPromoId, getItemTermByPromoId, getDiscountPromoByPromoId, getFreeGoodsPromoByPromoId } from '../../../api/EndPointPromotion';
import { promotionDataActions } from "../../../store/promotionData";
import { useParams } from "react-router-dom";
import { promotionItemActions } from "../../../store/promotionItem";

const DetailPromotion = () => {
    let [isLoading, setIsLoading] = useState(false);
    let dispatch = useDispatch();
    const params = useParams();

    let promoItemTemp = [
        {name: '', product_id: 0, product_code: ''}
    ]

    useEffect(() => {
        setIsLoading(true);
        getPromotionByPromoId(params.promotion_id).then((response) => {
            dispatch(promotionDataActions.promotionData(response.data.data))
        }).then(
            getItemTermByPromoId(params.promotion_id).then((response) => {
                response.data.data.map((value, index) => {
                    console.log(value.product_code)
                    promoItemTemp[index].product_code = value.product_code;
                    promoItemTemp[index].name = value.name;
                })
                dispatch(promotionItemActions.promoItem(promoItemTemp))
            })
        )
    }, [setIsLoading])

    if(isLoading === true){
        <p>Loading . . .</p>
    }

    return(
        <div className="mt-5">
            <Row className="mt-2 mx-1">
                <Col>
                    <PromoDataDetail/>
                </Col>
                <Col>
                    <PromoItemDetail/>
                    <PromoTypeDetail/>
                </Col>            
            </Row>
        </div>
    );
}

export default DetailPromotion;