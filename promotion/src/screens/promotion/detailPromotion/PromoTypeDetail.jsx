import { useSelector, useDispatch } from 'react-redux';
import { useParams } from "react-router-dom";
import { discountActions } from "../../../store/promoDiscount";
import { freeGoodsActions } from "../../../store/promoFreeGoods";
import { useEffect, useState } from "react";
import { getDiscountPromoByPromoId, getFreeGoodsPromoByPromoId } from '../../../api/EndPointPromotion';

import DiscountTabs from "./DiscountTabs";
import FreeGoodsTabs from "./FreeGoodsTabs";
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';

import './PromoTypeDetail.css'

const PromoTypeDetail = (props) => {
    let [isLoading, setIsLoading] = useState(false);
    let [type, setType] = useState("dis");
    let dispatch = useDispatch();
    const params = useParams();

    useEffect(() => {
        getDiscountPromoByPromoId(params.promotion_id).then((response) => {
            let data = response.data.data;
            if(data === null){
                setType("fg");
            }else{
                setType("dis");
                dispatch(discountActions.discountData(response.data.data))
            }
        }).then(
            getFreeGoodsPromoByPromoId(params.promotion_id).then((response) => {
                let data = response.data.data;
                if(data === null){
                    setIsLoading(false);
                }else{
                    setType("fg");
                    dispatch(freeGoodsActions.freeGoodsData(response.data.data));
                    setIsLoading(false);
                }
            })
        )
    }, [dispatch])

    if(type === "dis"){
        return(
            <Card className="type-card mt-4">
                <Row className="mx-1 my-1">
                    <DiscountTabs/>
                </Row>
            </Card>
            
        );
    }else if(type === "fg"){
        return(
            <Card className="type-card mt-4">
                <Row className="mx-1 my-1">
                    <FreeGoodsTabs/>
                </Row>
            </Card>
        );
    }else{
        return(
            <p>Loading . . .</p>
        );
    }
}

export default PromoTypeDetail;