import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useSelector } from 'react-redux';
import FormComponent from '../../../components/form/FormComponent';
import TextComponent from '../../../components/text/TextComponent';

const PromoDataDetail = () => {
    const promoData = useSelector((state) => {
        return state.promotionData.promotion;
    })

    return(
        <Card className="add-promotion-card">
            <Row className="mx-1 my-1">
                <Col>
                    <TextComponent className="add-promotion-text" content="Document Number" />
                    <FormComponent className="mb-3" 
                    controlId="documentNumber" type="text"  
                    placeholder= { promoData.document_number }
                    readOnly="yes"/>
                </Col>
                <Col>
                    <TextComponent className="add-promotion-text" content ="Minimum Purchase" />
                    <FormComponent className="mb-3" 
                    controlId="minimumPurchase" type="number"  
                    placeholder={ promoData.promo_value }
                    readOnly="yes" />
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                    <TextComponent className="add-promotion-text" content ="Start Date" />
                    <FormComponent className="mb-3" 
                    controlId="startDate" type="date"  
                    value={ promoData.start_date }
                    readOnly="yes" />
                </Col>
                <Col>
                    <TextComponent className="add-promotion-text" content ="End Date" />
                    <FormComponent className="mb-3" 
                    controlId="endDate" type="date"  
                    value={ promoData.end_date }
                    readOnly="yes" />
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                    <TextComponent className="add-promotion-text" content ="Description" />
                    <FormComponent className="mb-3" 
                    controlId="description" type="textfield"  
                    placeholder={ promoData.description } as = "textarea" rows = "3"
                    readOnly="yes" />
                </Col>
            </Row>
        </Card>
    );
}

export default PromoDataDetail;