import { FaSearch } from "react-icons/fa";
import { useSelector } from 'react-redux';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';

const DiscountTabs = () => {
    const promoDiscount=useSelector((state) => {
        return state.promoDiscount.discount;
    })
    console.log(promoDiscount);

    return(
        <Tabs defaultActiveKey="discount" id="uncontrolled-tab-example" >
            <Tab eventKey="discount" title="Discount" className="mt-1">
                <Row>
                    <Row>
                        <TextComponent className="add-promotion-text" content ="Discount" />
                    </Row>
                    <Row>
                        <Col sm={6}>
                            <FormComponent className="mb-3" 
                            controlId="discount" type="text"  
                            placeholder={ promoDiscount.discount_value } 
                            readOnly="yes" />
                        </Col>
                    </Row>
                </Row>
                <Row>
                    <Col>
                        <ButtonComponent className="custom-button-active" content="Submit"/>
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    );
}

export default DiscountTabs;