import { FaSearch } from "react-icons/fa";
import { useSelector } from 'react-redux';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';

const DiscountTabsEdit = (props) => {
    const promoDiscount=useSelector((state) => {
        return state.promoDiscountEdit.discount;
    })
    
    function discountHandler(events){
        props.setPromoDiscount({
            ...props.promoDiscount, 
            discount_value: events.target.value
        });
    }

    return(
        <Tabs defaultActiveKey="discount" id="uncontrolled-tab-example" >
            <Tab eventKey="discount" title="Discount" className="mt-1">
                <Row>
                    <Row>
                        <TextComponent className="add-promotion-text" content ="Discount" />
                    </Row>
                    <Row>
                        <Col sm={6}>
                            <FormComponent className="mb-3" 
                            controlId="discount" type="text"  
                            placeholder="discount"
                            defaultValue={ promoDiscount.discount_value } 
                            onChange={ discountHandler }
                            />
                        </Col>
                    </Row>
                </Row>
                <Row>
                    <Col>
                        <ButtonComponent className="custom-button-active" content="Submit" onClick={ props.editPromoDiscount }/>
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    );
}

export default DiscountTabsEdit;