import { FaSearch } from "react-icons/fa";
import { useSelector } from 'react-redux';
import { getItemCodeSearch } from '../../../api/EndPointPromotion';
import { useEffect, useState } from "react";

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

const FreeGoodsTabsEdit = (props) => {
    const promoFreeGoods=useSelector((state) => {
        return state.promoFreeGoodsEdit.freeGoods;
    })

    let [itemName, setItemName] = useState({
        name: props.promoFreeGoods.name
    })

    function freeGoodsNameHandler(itemName){
        props.setPromoFreeGoods({
            ...props.promoFreeGoods, name: itemName
        });
    }

    function freeGoodsQuantityHandler(events){
        props.setPromoFreeGoods({
            ...props.promoFreeGoods, quantity: events.target.value
        });
    }

    function freeGoodsCodeHandler(events){
        props.setPromoFreeGoods({
            ...props.promoFreeGoods, product_code: events.target.value
        });
    }

    function searchItemHandler(events){
        events.preventDefault();
        getItemCodeSearch(props.promoFreeGoods.product_code).then((response) => {
            freeGoodsNameHandler(response.data.data.name);
            setItemName({
                ...itemName, name:response.data.data.name
            })
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    return(
        <Tabs defaultActiveKey="free-goods" id="uncontrolled-tab-example" >
            <Tab eventKey="free-goods" title="Free Goods" className="mt-1">
                <Row>
                    <Col>
                        <Row>
                            <Col md={3}>
                                <TextComponent className="add-promotion-text" content ="Product Name" />
                            </Col>
                            <Col md={3}>
                                <TextComponent className="add-promotion-text" content ="Item Quantity" />
                            </Col>
                            <Col md={3}>
                                <TextComponent className="add-promotion-text" content ="Product Code" />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={3}>
                                <FormComponent className="mb-3" 
                                controlId="promoItem" type="text"  
                                placeholder={ itemName.name }
                                readOnly="yes" />
                            </Col>
                            <Col md={3}>
                                <FormComponent className="mb-3" 
                                controlId="promoItem" type="number"  
                                placeholder="quantity"
                                defaultValue={ promoFreeGoods.quantity }
                                onChange={ freeGoodsQuantityHandler }/>
                            </Col>
                            <Col md={3}>
                                <FormComponent className="mb-3" 
                                controlId="promoItem" type="text"  
                                placeholder="product code"
                                defaultValue={ promoFreeGoods.product_code }
                                onChange={ freeGoodsCodeHandler }
                                />
                            </Col>
                            <Col xs="auto">
                                <Button variant="primary" className="custom-promo-item-button" onClick={ searchItemHandler }>
                                    <FaSearch className="custom-icon-promotion" size="22px" />
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ButtonComponent className="custom-button-active" content="Submit" onClick={ props.editPromoFreeGoods }/>
                    </Col>
                </Row>
            </Tab>
        </Tabs>
    );
}

export default FreeGoodsTabsEdit;