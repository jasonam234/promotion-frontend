import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { promotionDataActions } from "../../../store/promotionData";
import { useParams } from "react-router-dom";
import { updateDiscountPromo, updateFreeGoodsPromo, getPromotionByPromoId, getItemTermByPromoId, postItemTerm, updateItemTerm } from '../../../api/EndPointPromotion';

import PromoDataEdit from './PromoDataEdit';
import PromoItemEdit from './PromoItemEdit';
import PromoTypeEdit from './PromoTypeEdit';
import Moment from 'moment';
import Swal from 'sweetalert2';
import { promotionItemEditActions } from '../../../store/promotionItemEdit';

const EditPromotion = () => {
    let [isLoading, setIsLoading] = useState(false);
    let dispatch = useDispatch();
    const params = useParams();

    const promoDataApi = useSelector((state) => {
        return state.promotionData.promotion;
    })

    useEffect(() => {
        setIsLoading(true);
        getPromotionByPromoId(params.promotion_id).then((response) => {
            dispatch(promotionDataActions.promotionData(response.data.data));
            setPromoData(response.data.data);
        }).then(
            getItemTermByPromoId(params.promotion_id).then((response) => {
                console.log(response.data.data)
                response.data.data.map((value, index) => {
                    promoItemTemp[index].product_code = value.product_code;
                    promoItemTemp[index].name = value.name;
                    promoItemTemp[index].product_id = value.product_id;
                    promoItemTemp[index].term_id = value.term_id;
                })
                dispatch(promotionItemEditActions.promoItemEdit(promoItemTemp));
                setPromoItemTermOne({
                    ...promoItemTermOne, 
                    product_code: promoItemTemp[0].product_code,
                    name:promoItemTemp[0].name,
                    term_id: promoItemTemp[0].term_id, 
                    product_id: promoItemTemp[0].product_id
                });
                setPromoItemTermTwo({
                    ...promoItemTermOne, 
                    product_code: promoItemTemp[1].product_code,
                    name:promoItemTemp[1].name,
                    term_id: promoItemTemp[1].term_id, 
                    product_id: promoItemTemp[1].product_id
                });
                setPromoItemTermThree({
                    ...promoItemTermOne, 
                    product_code: promoItemTemp[2].product_code,
                    name:promoItemTemp[2].name,
                    term_id: promoItemTemp[2].term_id, 
                    product_id: promoItemTemp[2].product_id
                });
                setIsLoading(false);
            })
        )
    }, [dispatch])

    let [promoData, setPromoData] = useState({});

    let promoItemTemp = [
        {name: 'product name', product_id: 0, product_code: '', term_id: 0},
        {name: 'product name', product_id: 0, product_code: '', term_id: 0},
        {name: 'product name', product_id: 0, product_code: '', term_id: 0}
    ]

    let[promoItemTermOne,setPromoItemTermOne] = useState({
        name: '', 
        term_id: 0, 
        product_id: 0, 
        product_code: ''
    })
    console.log(promoItemTermOne)

    let[promoItemTermTwo,setPromoItemTermTwo] = useState({
        name: '', 
        term_id: 0, 
        product_id: 0, 
        product_code: ''
    })
    console.log(promoItemTermTwo)

    let[promoItemTermThree,setPromoItemTermThree] = useState({
        name: '', 
        term_id: 0, 
        product_id: 0, 
        product_code: ''
    })
    console.log(promoItemTermThree)

    let[promoDiscount, setPromoDiscount] = useState({
        promo_disc_id: 0, 
        discount_value: 0
    });

    let[promoFreeGoods, setPromoFreeGoods] = useState({
        name: '', 
        product_id: 0, 
        quantity: 0,
        product_code: '', 
        promotion_free_goods_id: 0
    });

    function editPromoDiscount(events){
        let obj={
            "promo_disc_id": promoDiscount.promo_disc_id,
            "promo": {
              "promotion_id": promoData.promotion_id,
              "document_number": promoData.document_number,
              "description": promoData.description,
              "start_date": Moment(promoData.start_date).format('YYYY-MM-DD'),
              "end_date": Moment(promoData.end_date).format('YYYY-MM-DD'),
              "promo_value": promoData.promo_value,
            },
            "discount_value": promoDiscount.discount_value
        }
        
        if(promoDiscount.discount_value > 80){
            Swal.fire({
                icon: 'error',
                text: 'Discount Must be Lower Than 80% !'
            })
        }else{
            updateDiscountPromo(obj).then(
                Swal.fire({
                    icon: 'success',
                    text: 'Promotion Updated Successfully !'
                })
            ).catch(() => {
                Swal.fire({
                    icon: 'error',
                    text: 'Post Promo Discount Error !'
                })
            })
        }
    }

    function editPromoFreeGoods(events){
        events.preventDefault();
        let obj = {
            "promotion_free_goods_id": promoFreeGoods.promotion_free_goods_id,
            "product": {
                "product_id": promoFreeGoods.product_id
            },
            "promo": {
                "promotion_id": promoData.promotion_id,
                "document_number": promoData.document_number,
                "description": promoData.description,
                "start_date": Moment(promoData.start_date).format('YYYY-MM-DD'),
                "end_date": Moment(promoData.end_date).format('YYYY-MM-DD'),
                "promo_value": promoData.promo_value,
            },
            "quantity": promoFreeGoods.quantity,
            "product_code": promoFreeGoods.product_code
        }
        
        updateFreeGoodsPromo(obj).then(
            Swal.fire({
                icon: 'success',
                text: 'Post Promo Discount Successful !'
            })
        ).catch(() => {
            Swal.fire({
                icon: 'error',
                text: 'Post Promo Discount Error !'
            })
        })
    }

    function editPromoItemTermOne(events){
        events.preventDefault();
        let obj = {
            "term_id": promoItemTermOne.term_id,
            "product": {
                "product_id": promoItemTermOne.product_id
            }
        }
        console.log(obj);
        if(obj.term_id == 0){
            postItemTerm(obj).then(
                Swal.fire({
                    icon: 'success',
                    text: 'Item One Posted Successfully !'
                })
            ).catch(
                Swal.fire({
                    icon: 'error',
                    text: 'Item One Post Terminated !'
                })
            )
        }else{
            updateItemTerm(obj).then((response) => {
                Swal.fire({
                    icon: 'success',
                    text: 'Item One Updated Successfully !'
                })
            }).catch((e)=>{
                console.log(e)
                Swal.fire({
                    icon: 'error',
                    text: 'Item One Update Terminated !'
                })
            })
        }
    }

    if(isLoading === true){
        return(
            <p>Loading . . .</p>
        );
    }

    return(
        <div className='mt-5'>
            <Row className="mt-2 mx-1">
                <Col>
                    <PromoDataEdit promoData={ promoData } setPromoData={ setPromoData }/>
                </Col>
                <Col>
                    <PromoItemEdit promoItemTermOne={ promoItemTermOne } setPromoItemTermOne={ setPromoItemTermOne }
                        editPromoItemTermOne={ editPromoItemTermOne }/>
                    <PromoTypeEdit promoDiscount={ promoDiscount } setPromoDiscount={ setPromoDiscount } 
                    promoFreeGoods={ promoFreeGoods } setPromoFreeGoods={ setPromoFreeGoods }
                    editPromoDiscount={ editPromoDiscount } editPromoFreeGoods={ editPromoFreeGoods }/>
                </Col>            
            </Row>
        </div>
    );
}

export default EditPromotion;