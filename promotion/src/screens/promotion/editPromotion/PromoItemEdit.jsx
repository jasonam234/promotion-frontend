import { FaSearch } from "react-icons/fa";
import { useEffect, useState } from 'react';
import { getItemCodeSearch } from '../../../api/EndPointPromotion';
import { useSelector } from 'react-redux';
import { MdRefresh } from "react-icons/md";

import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

import './PromoItemEdit.css'

const PromoItemEdit = (props) => {
    let [isLoading, setIsLoading] = useState(false);

    const promoItem = useSelector((state) => {
        return state.promoItemEdit.item;
    })

    let [placeholderOne, setPlaceholderOne] = useState({
        itemNameOne:promoItem[0].name
    });

    let [placeholderTwo, setPlaceholderTwo] = useState({
        itemNameTwo:promoItem[1].name
    });

    let [placeholderThree, setPlaceholderThree] = useState({
        itemNameThree:promoItem[2].name
    });

    function itemNameOneHandler(itemName){
        setPlaceholderOne({
            ...props.placeholderOne, itemNameOne: itemName
        });
    }

    function itemNameTwoHandler(itemName){
        setPlaceholderTwo({
            ...props.placeholderTwo, itemNameTwo: itemName
        });
    }

    function itemNameThreeHandler(itemName){
        setPlaceholderThree({
            ...props.placeholderThree, itemNameThree: itemName
        });
    }

    function itemOneIdHandler(productId){
        props.setPromoItemTermOne({
            ...props.promoItemTermOne, product_id: productId
        });
    }

    function itemTwoIdHandler(productId){
        props.setPromoItemTermTwo({
            ...props.promoItemTermTwo, product_id: productId
        });
    }

    function itemThreeIdHandler(productId){
        props.setPromoItemTermThree({
            ...props.promoItemTermThree, product_id: productId
        });
    }

    function itemCodeOneHandler(events){
        props.setPromoItemTermOne({
            ...props.promoItemTermOne, product_code: events.target.value
        });
    }

    function itemCodeTwoHandler(events){
        props.setPromoItemTermTwo({
            ...props.promoItemTermTwo, product_code: events.target.value
        });
    }

    function itemCodeThreeHandler(events){
        props.setPromoItemTermThree({
            ...props.promoItemTermThree, product_code: events.target.value
        });
    }

    function searchItemHandlerOne(events){
        events.preventDefault();
        getItemCodeSearch(props.promoItemTermOne.product_code).then((response) => {
            itemOneIdHandler(response.data.data.product_id);
            itemNameOneHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            itemOneIdHandler(0);
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    function searchItemHandlerTwo(events){
        events.preventDefault();
        getItemCodeSearch(props.promoItemTermTwo.product_code).then((response) => {
            itemTwoIdHandler(response.data.data.product_id);
            itemNameTwoHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            itemTwoIdHandler(0);
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }
    
    function searchItemHandlerThree(events){
        events.preventDefault();
        getItemCodeSearch(props.promoItemTermThree.product_code).then((response) => {
            itemThreeIdHandler(response.data.data.product_id);
            itemNameThreeHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            itemThreeIdHandler(0);
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    if(isLoading === true){
        return(
            <p>Loading . . .</p>
        );
    }

    return(
        <Card className="item-card">
            <Row className="mx-1 my-1">
                <Col>
                    <Row>
                        <Col sm={5}>
                            <TextComponent className="add-promotion-text-required" content ="Promo Item #1" />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={4}>
                            <FormComponent className="mb-3" 
                            controlId="promoItem" type="text"  
                            placeholder={ placeholderOne.itemNameOne }
                            onChange={ itemNameOneHandler } readOnly="yes" />
                        </Col>
                        <Col sm={4}>
                            <FormComponent className="mb-3" 
                            controlId="promoItem" type="text"  
                            placeholder="product code"
                            defaultValue={ props.promoItemTermOne.product_code }
                            onChange={ itemCodeOneHandler }/>
                        </Col>
                        <Col>
                            <Button variant="primary" className="custom-promo-item-button" onClick={ searchItemHandlerOne }>
                                <FaSearch className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                        <Col>
                            <Button variant="warning" className="custom-promo-item-button" onClick={ props.editPromoItemTermOne }>
                                <MdRefresh className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                <Row>
                    <Col sm={5}>
                            <TextComponent className="add-promotion-text visually-hidden" content ="Promo Item #2" />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={4}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder={ placeholderTwo.itemNameTwo }
                            onChange={ itemNameTwoHandler } readOnly="yes" />
                        </Col>
                        <Col sm={4}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder="product code"
                            defaultValue={ promoItem[1].product_code }
                            onChange={ itemCodeTwoHandler }/>
                        </Col>
                        <Col>
                            <Button variant="primary" className="custom-promo-item-button visually-hidden" onClick={ searchItemHandlerTwo }>
                                <FaSearch className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                        <Col>
                            <Button variant="warning" className="custom-promo-item-button visually-hidden" onClick={ props.editPromoItemTermTwo }>
                                <MdRefresh className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mx-1">
                <Col className="mx-1">
                <Row>
                    <Col sm={5}>
                        <TextComponent className="add-promotion-text visually-hidden" content ="Promo Item #3" />
                    </Col>
                    </Row>
                    <Row>
                        <Col sm={4}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder={ placeholderThree.itemNameThree }
                            onChange={ itemNameThreeHandler } readOnly="yes" />
                        </Col>
                        <Col sm={4}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder="product code"
                            defaultValue={ promoItem[2].product_code } 
                            onChange={ itemCodeThreeHandler }/>
                        </Col>
                        <Col>
                            <Button variant="primary" className="custom-promo-item-button visually-hidden" onClick={ searchItemHandlerThree }>
                                <FaSearch className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                        <Col>
                            <Button variant="warning" className="custom-promo-item-button visually-hidden" onClick={ props.editPromoItemTermThree }>
                                <MdRefresh className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Card>
    );
}

export default PromoItemEdit;