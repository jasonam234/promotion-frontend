import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import TextComponent from '../../../components/text/TextComponent';

import { useSelector } from 'react-redux';

const PromoDataEdit = (props) => {
    const promoData = useSelector((state) => {
        return state.promotionData.promotion;
    })

    function documentNumberHandler(events){
        props.setPromoData({
            ...props.promoData, document_number: events.target.value
        });
    }

    function minimumPurchaseHandler(events){
        props.setPromoData({
            ...props.promoData, promo_value: events.target.value
        });
    }

    function startDateHandler(events){
        props.setPromoData({
            ...props.promoData, start_date: events.target.value
        });
    }
    
    function endDateHandler(events){
        props.setPromoData({
            ...props.promoData, end_date: events.target.value
        });
    }

    function descriptionHandler(events){
        props.setPromoData({
            ...props.promoData, description: events.target.value
        });
    }
    
    return(
        <Card className="add-promotion-card">
            <Row className="mx-1 my-1">
                <Col>
                    <TextComponent className="add-promotion-text" content="Document Number" />
                    <FormComponent className="mb-3" 
                    controlId="documentNumber" type="text"  
                    onChange={ documentNumberHandler } 
                    placeholder= { promoData.document_number }
                    readOnly="yes"/>
                </Col>
                <Col>
                    <TextComponent className="add-promotion-text" content ="Minimum Purchase" />
                    <FormComponent className="mb-3" 
                    controlId="minimumPurchase" type="number"  
                    defaultValue={ promoData.promo_value }
                    onChange={ minimumPurchaseHandler } />
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                    <TextComponent className="add-promotion-text" content ="Start Date" />
                    <FormComponent className="mb-3" 
                    controlId="startDate" type="date"  
                    defaultValue={ promoData.start_date }
                    onChange={ startDateHandler }/>
                </Col>
                <Col>
                    <TextComponent className="add-promotion-text" content ="End Date" />
                    <FormComponent className="mb-3" 
                    controlId="endDate" type="date"  
                    defaultValue={ promoData.end_date }
                    onChange={ endDateHandler }/>
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                    <TextComponent className="add-promotion-text" content ="Description" />
                    <FormComponent className="mb-3" 
                    controlId="description" type="textfield"  
                    defaultValue={ promoData.description } as = "textarea" rows = "3"
                    onChange={ descriptionHandler }/>
                </Col>
            </Row>
        </Card>
    );
}

export default PromoDataEdit;