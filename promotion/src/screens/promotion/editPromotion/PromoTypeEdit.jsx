import { useSelector, useDispatch } from 'react-redux';
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { getDiscountPromoByPromoId, getFreeGoodsPromoByPromoId } from '../../../api/EndPointPromotion';
import { discountEditActions } from '../../../store/promoDiscountEdit';
import { freeGoodsEditActions } from '../../../store/promoFreeGoodsEdit';

import FreeGoodsTabsEdit from "./FreeGoodsTabsEdit";
import DiscountTabsEdit from "./DiscountTabsEdit";
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';

import './PromoTypeEdit.css';

const PromoTypeEdit = (props) => {
    let [isLoading, setIsLoading] = useState(false);
    let [type, setType] = useState("dis");
    let dispatch = useDispatch();
    const params = useParams();

    useEffect(() => {
        getDiscountPromoByPromoId(params.promotion_id).then((response) => {
            let data = response.data.data;
            if(data === null){
                setType("fg");
            }else{
                setType("dis");
                dispatch(discountEditActions.discountEditData(response.data.data))
                props.setPromoDiscount({
                    ...props.promoDiscount, 
                    discount_value: response.data.data.discount_value,
                    promo_disc_id: response.data.data.promo_disc_id
                });
            }
        }).then(
            getFreeGoodsPromoByPromoId(params.promotion_id).then((response) => {
                let data = response.data.data;
                console.log(data)
                if(data === null){
                    setIsLoading(false);
                }else{
                    setType("fg");
                    dispatch(freeGoodsEditActions.freeGoodsEditData(response.data.data));
                    props.setPromoFreeGoods({
                        ...props.promoFreeGoods,
                        name: response.data.data.name, 
                        product_id: response.data.data.product_id, 
                        quantity: response.data.data.quantity,
                        product_code: response.data.data.product_code, 
                        promotion_free_goods_id: response.data.data.promotion_free_goods_id
                    })
                    setIsLoading(false);
                }
            })
        )
    }, [dispatch])


    if(type === "dis"){
        return(
            <Card className="type-card mt-4">
                <Row className="mx-1 my-1">
                    <DiscountTabsEdit promoDiscount={ props.promoDiscount } setPromoDiscount={ props.setPromoDiscount } 
                    editPromoDiscount={ props.editPromoDiscount }/>
                </Row>
            </Card>
            
        );
    }else if(type === "fg"){
        return(
            <Card className="type-card mt-4">
                <Row className="mx-1 my-1">
                    <FreeGoodsTabsEdit promoFreeGoods={ props.promoFreeGoods } setPromoFreeGoods={ props.setPromoFreeGoods }
                    editPromoFreeGoods={ props.editPromoFreeGoods }/>
                </Row>
            </Card>
            
        );
    }else{
        return(
            <p>Loading . . .</p>
        );
    }
}

export default PromoTypeEdit;