import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider, PaginationListStandalone } from 'react-bootstrap-table2-paginator';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';

import { useSelector, useDispatch } from "react-redux";
import { FaTrash, FaPen, FaEye } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import { deletePromotion, postPromoSearch } from '../../api/EndPointPromotion';
import { useState, useEffect } from "react";
import { promotionActions } from "../../store/promotion";

import './PromotionTable.css'

const PromotionTable = () => {
    let dispatch = useDispatch();

    let promotions = useSelector((state) => {
        return state.promotion.promotion;
    })

    console.log(promotions)

    let [data, setData] = useState({
        product_id:"",
        document_number:""
    });

    function deleteHandler(promotion_id){
        Swal.fire({
            title: 'Do you want to delete this promotion?',
            showDenyButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: `No`,
        }).then((result) => {
            if (result.isConfirmed) {
                deletePromotion(promotion_id).then((response) => {
                    Swal.fire({
                        title: 'Promotion Deleted !',
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if(result.isConfirmed){
                            postPromoSearch(data).then((response) => {
                                dispatch(promotionActions.allPromotion(response.data.data))
                            })
                        }
                    })
                })
                
            }
        })
    }

    function buttonFormatter(cell){
        return(
            <div>
                <Link to={`/promotion/promotion_detail/${cell}`}>
                    <Button variant="primary" className="custom-table-button" size="sm">
                        <FaEye className="custom-icon-promotion" size="8px" />
                    </Button>
                </Link>
                <Link to={`/promotion/promotion_edit/${cell}`}>
                    <Button variant="warning" className="custom-table-button" size="sm">
                        <FaPen className="custom-icon-promotion" size="8px" />
                    </Button>
                </Link>
                <Button variant="danger" className="custom-table-button" size="sm" onClick={ () => deleteHandler(cell) }>
                    <FaTrash className="custom-icon-promotion" size="8px" />
                </Button>
            </div>

        );
    }

    function priceFormatter(cell) {      
        return (
            <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(cell)}</span>
        );
    }

    const options = {
        sizePerPage: 10,
        hideSizePerPage: true,
        hidePageListOnlyOnePage: true,
        pageStartIndex: 1,
        firstPageText: 'First',
        prePageText: 'Back',
        nextPageText: 'Next',
        lastPageText: 'Last',
        nextPageTitle: 'First page',
        prePageTitle: 'Pre page',
        firstPageTitle: 'Next page',
        lastPageTitle: 'Last page',
        totalSize: promotions.size
    };

    const columns = [{
        dataField: 'promotion_id',
        text: 'Promotion ID',
        sort: true,
        hidden: true
        },
        { dataField: 'document_number',
        text: 'Document Number'
        }, 
        { dataField: 'promo_value',
        formatter: priceFormatter,
        headerAlign: 'right',
        align: 'right',
        text: 'Promo Value'
        },
        { dataField: 'name',
        text: 'Promo Item'
        }, 
        { dataField: 'start_date',
        headerAlign: 'right',
        align: 'right',
        text: 'Start Date'
        }, 
        { dataField: 'end_date',
        headerAlign: 'right',
        align: 'right',
        text: 'End Date'
        }, 
        { dataField: 'last_edit',
        headerAlign: 'right',
        align: 'right',
        text: 'Last Edited'
        }
        , { dataField: 'promotion_id',
        formatter: buttonFormatter,
        text: 'Action'
    }];

    const defaultSorted = [{
        dataField: 'promotion_id',
        order: 'desc'
    }];

    return(
        <div className="scrollTable">
            <PaginationProvider
            pagination={ paginationFactory(options) }
            >
            {
                ({
                paginationProps,
                paginationTableProps
                }) => (
                <div>
                    <BootstrapTable
                    isKey
                    keyField="promotions"
                    data={ promotions }
                    columns={ columns }
                    { ...paginationTableProps }
                    defaultSorted={ defaultSorted } 
                    />
                </div>
                )
            }
            </PaginationProvider>
        </div>
    );
}

export default PromotionTable;