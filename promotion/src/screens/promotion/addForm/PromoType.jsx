import { FaSearch } from "react-icons/fa";
import { getItemCodeSearch } from '../../../api/EndPointPromotion';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

import './PromoType.css'

const PromoType = (props) => {
    function discountHandler(events){
        props.setPromoType({
            ...props.promoType, discount: events.target.value
        });
    }

    function freeGoodsNameHandler(itemName){
        props.setPromoType({
            ...props.promoType, freeGoodsName: itemName
        });
    }

    function freeGoodsQuantityHandler(events){
        props.setPromoType({
            ...props.promoType, freeGoodsQuantity: events.target.value
        });
    }

    function freeGoodsCodeHandler(events){
        props.setPromoType({
            ...props.promoType, freeGoodsCode: events.target.value
        });
    }

    function searchItemHandler(events){
        events.preventDefault();
        getItemCodeSearch(props.promoType.freeGoodsCode).then((response) => {
            freeGoodsNameHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    return(
        <Card className="type-card mt-4">
            <Row className="mx-1 my-1">
                <Tabs defaultActiveKey="discount" id="uncontrolled-tab-example" >
                    <Tab eventKey="discount" title="Discount" className="mt-1">
                        <Row>
                            <Row>
                                <TextComponent className="add-promotion-text" content ="Discount Value" />
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    <FormComponent className="mb-3" 
                                    controlId="discount" type="text"  
                                    placeholder="discount" 
                                    onChange={ discountHandler }/>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Col>
                                <ButtonComponent className="custom-button-active" content="Submit" onClick={ props.addPromoDiscount }/>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab eventKey="free-goods" title="Free Goods" className="mt-1">
                        <Row>
                            <Col>
                                <Row>
                                    <Col md={3}>
                                        <TextComponent className="add-promotion-text" content ="Product Name" />
                                    </Col>
                                    <Col md={3}>
                                        <TextComponent className="add-promotion-text" content ="Item Quantity" />
                                    </Col>
                                    <Col md={3}>
                                        <TextComponent className="add-promotion-text" content ="Product Code" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <FormComponent className="mb-3" 
                                        controlId="promoItem" type="text"  
                                        placeholder={ props.promoType.freeGoodsName }
                                        onChange={ freeGoodsNameHandler } readOnly="yes" />
                                    </Col>
                                    <Col md={3}>
                                        <FormComponent className="mb-3" 
                                        controlId="promoItem" type="number"  
                                        placeholder="item quantity"
                                        onChange={ freeGoodsQuantityHandler }/>
                                    </Col>
                                    <Col md={3}>
                                        <FormComponent className="mb-3" 
                                        controlId="promoItem" type="text"  
                                        placeholder="product code" 
                                        onChange={ freeGoodsCodeHandler }/>
                                    </Col>
                                    <Col xs="auto">
                                        <Button variant="primary" className="custom-promo-item-button" onClick={ searchItemHandler }>
                                            <FaSearch className="custom-icon-promotion" size="22px" />
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <ButtonComponent className="custom-button-active" content="Submit" onClick={ props.addPromoFreeGoods }/>
                            </Col>
                        </Row>
                    </Tab>
                </Tabs>
            </Row>
        </Card>
    );
}

export default PromoType;