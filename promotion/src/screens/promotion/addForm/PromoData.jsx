import { useState } from 'react';

import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import TextComponent from '../../../components/text/TextComponent';

const PromoData = (props) => {
    function documentNumberHandler(events){
        props.setPromoData({
            ...props.promoData, documentNumber: events.target.value
        });
    }

    function minimumPurchaseHandler(events){
        props.setPromoData({
            ...props.promoData, promoValue: events.target.value
        });
    }

    function startDateHandler(events){
        props.setPromoData({
            ...props.promoData, startDate: events.target.value
        });
    }
    
    function endDateHandler(events){
        props.setPromoData({
            ...props.promoData, endDate: events.target.value
        });
    }

    function descriptionHandler(events){
        props.setPromoData({
            ...props.promoData, description: events.target.value
        });
    }

    return(
        <Card className="add-promotion-card">
            <Row className="mx-1 my-1">
                <Col>
                    <TextComponent className="add-promotion-text" content="Document Number" />
                    <FormComponent className="mb-3" 
                    controlId="documentNumber" type="text"  
                    placeholder="document number"
                    isRequired="yes" onChange={ documentNumberHandler }/>
                </Col>
                <Col>
                    <TextComponent className="add-promotion-text" content ="Minimum Purchase" />
                    <FormComponent className="mb-3" 
                    controlId="minimumPurchase" type="number"  
                    placeholder="minimum purchase"
                    isRequired="yes" onChange={ minimumPurchaseHandler } />
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                    <TextComponent className="add-promotion-text" content ="Start Date" />
                    <FormComponent className="mb-3" 
                    controlId="startDate" type="date"  
                    placeholder="start date" 
                    isRequired="yes" onChange={ startDateHandler } />
                </Col>
                <Col>
                    <TextComponent className="add-promotion-text" content ="End Date" />
                    <FormComponent className="mb-3" 
                    controlId="endDate" type="date"  
                    placeholder="end date" 
                    isRequired="yes" onChange={ endDateHandler } />
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                    <TextComponent className="add-promotion-text" content ="Description" />
                    <FormComponent className="mb-3" 
                    controlId="description" type="textfield"  
                    placeholder="description" as = "textarea" rows = "3"
                    isRequired="yes" onChange={ descriptionHandler } />
                </Col>
            </Row>
        </Card>
    );
}

export default PromoData;