import { FaSearch } from "react-icons/fa";
import { useEffect, useState } from 'react';
import { getItemCodeSearch } from '../../../api/EndPointPromotion';

import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import FormComponent from '../../../components/form/FormComponent';
import ButtonComponent from '../../../components/button/ButtonComponent';
import TextComponent from '../../../components/text/TextComponent';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

import './PromoItem.css'

const PromoItem = (props) => {
    
    let [placeholderOne, setPlaceholderOne] = useState({
        itemNameOne:"product name"
    });

    let [placeholderTwo, setPlaceholderTwo] = useState({
        itemNameTwo:"product name"
    });


    let [placeholderThree, setPlaceholderThree] = useState({
        itemNameThree:"product name"
    });

    function itemNameOneHandler(itemName){
        setPlaceholderOne({
            ...props.placeholderOne, itemNameOne: itemName
        });
    }

    function itemNameTwoHandler(itemName){
        setPlaceholderTwo({
            ...props.placeholderTwo, itemNameTwo: itemName
        });
    }

    function itemNameThreeHandler(itemName){
        setPlaceholderThree({
            ...props.placeholderThree, itemNameThree: itemName
        });
    }

    function itemOneIdHandler(productId){
        props.setPromoItem({
            ...props.promoItem, itemOneId: productId
        });
    }

    function itemTwoIdHandler(productId){
        props.setPromoItem({
            ...props.promoItem, itemTwoId: productId
        });
    }

    function itemThreeIdHandler(productId){
        props.setPromoItem({
            ...props.promoItem, itemThreeId: productId
        });
    }

    function itemCodeOneHandler(events){
        props.setPromoItem({
            ...props.promoItem, itemCodeOne: events.target.value
        });
    }

    function itemCodeTwoHandler(events){
        props.setPromoItem({
            ...props.promoItem, itemCodeTwo: events.target.value
        });
    }

    function itemCodeThreeHandler(events){
        props.setPromoItem({
            ...props.promoItem, itemCodeThree: events.target.value
        });
    }

    function searchItemHandlerOne(events){
        events.preventDefault();
        getItemCodeSearch(props.promoItem.itemCodeOne).then((response) => {
            itemOneIdHandler(response.data.data.product_id);
            itemNameOneHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            itemOneIdHandler(0);
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    
    function searchItemHandlerTwo(events){
        events.preventDefault();
        console.log(props.promoItem.itemCodeTwo);
        getItemCodeSearch(props.promoItem.itemCodeTwo).then((response) => {
            itemTwoIdHandler(response.data.data.product_id);
            itemNameTwoHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            itemTwoIdHandler(0);
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    
    function searchItemHandlerThree(events){
        events.preventDefault();
        getItemCodeSearch(props.promoItem.itemCodeThree).then((response) => {
            itemThreeIdHandler(response.data.data.product_id);
            itemNameThreeHandler(response.data.data.name);
            Swal.fire({
                icon: 'success',
                text: 'Item Found !'
            })
        }).catch(() => {
            itemThreeIdHandler(0);
            Swal.fire({
                icon: 'error',
                text: 'Item Not Found !'
            })
        })
    }

    return(
        <Card className="item-card">
            <Row className="mx-1 my-1">
                <Col>
                    <Row>
                        <Col sm={5}>
                            <TextComponent className="add-promotion-text-required" content ="Promo Item #1" />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={5}>
                            <FormComponent className="mb-3" 
                            controlId="promoItem" type="text"  
                            placeholder={ placeholderOne.itemNameOne }
                            onChange={ itemNameOneHandler } readOnly="yes" />
                        </Col>
                        <Col sm={5}>
                            <FormComponent className="mb-3" 
                            controlId="promoItem" type="text"  
                            placeholder="product code" 
                            onChange={ itemCodeOneHandler }/>
                        </Col>
                        <Col>
                            <Button variant="primary" className="custom-promo-item-button" onClick={ searchItemHandlerOne }>
                                <FaSearch className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mx-1">
                <Col>
                <Row>
                    <Col sm={5}>
                            <TextComponent className="add-promotion-text visually-hidden" content ="Promo Item #2" />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={5}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder={ placeholderTwo.itemNameTwo }
                            onChange={ itemNameTwoHandler } readOnly="yes" />
                        </Col>
                        <Col sm={5}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder="product code"
                            onChange={ itemCodeTwoHandler }/>
                        </Col>
                        <Col>
                            <Button variant="primary" className="custom-promo-item-button visually-hidden" onClick={ searchItemHandlerTwo }>
                                <FaSearch className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mx-1">
                <Col className="mx-1">
                <Row>
                    <Col sm={5}>
                        <TextComponent className="add-promotion-text visually-hidden" content ="Promo Item #3" />
                    </Col>
                    </Row>
                    <Row>
                        <Col sm={5}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder={ placeholderThree.itemNameThree }
                            onChange={ itemNameThreeHandler } readOnly="yes" />
                        </Col>
                        <Col sm={5}>
                            <FormComponent className="mb-3 visually-hidden" 
                            controlId="promoItem" type="text"  
                            placeholder="product code" 
                            onChange={ itemCodeThreeHandler }/>
                        </Col>
                        <Col>
                            <Button variant="primary" className="custom-promo-item-button visually-hidden" onClick={ searchItemHandlerThree }>
                                <FaSearch className="custom-icon-promotion" size="22px" />
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Card>
    );
}

export default PromoItem;