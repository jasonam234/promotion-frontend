import { useEffect, useState } from 'react';
import { postPromoDiscount, postPromoFreeGoods, postItemTerm } from '../../../api/EndPointPromotion';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import PromoItem from './PromoItem';
import PromoType from './PromoType';
import PromoData from './PromoData';
import Moment from 'moment';
import Swal from 'sweetalert2';

import './AddPromotion.css'

const AddPromotion = () => {
    let [promoData, setPromoData] = useState({
        documentNumber:"",
        promoValue:0,
        startDate:"",
        endDate:"",
        description:""
    });

    let [promoItem, setPromoItem] = useState({
        itemOneId:0,
        itemCodeOne:"",
        itemTwoId:0,
        itemCodeTwo:"",
        itemThreeId:0,
        itemCodeThree:""
    })

    let[promoType, setPromoType] = useState({
        discount:"",
        freeGoodsQuantity:"",
        freeGoodsName:"product name",
        freeGoodsCode:""
    })

    function addPromoDiscount(events){
        events.preventDefault();
        let obj={
            "promo": {
                "user": {
                "user_id": localStorage.getItem("user_id")
                },
                "document_number": promoData.documentNumber,
                "description": promoData.description,
                "start_date": Moment(promoData.startDate).format('YYYY-MM-DD'),
                "end_date": Moment(promoData.endDate).format('YYYY-MM-DD'),
                "promo_value": promoData.promoValue
            },
            "discount_value": promoType.discount
        }
        if(parseInt(promoType.discount) > 80){
            Swal.fire({
                icon: 'error',
                text: 'Discount Must be Lower Than 80% !'
            })
        }else{
            postPromoDiscount(obj).then((response) => {
                console.log(response.data.data.promotion_id)
                let itemTermOne={
                    "product": {
                        "product_id": promoItem.itemOneId
                    },
                    "promo": {
                        "promotion_id": response.data.data.promotion_id
                    }
                }
                let itemTermTwo={
                    "product": {
                        "product_id": promoItem.itemTwoId
                    },
                    "promo": {
                        "promotion_id": response.data.data.promotion_id
                    }
                }
                let itemTermThree={
                    "product": {
                        "product_id": promoItem.itemThreeId
                    },
                    "promo": {
                        "promotion_id": response.data.data.promotion_id
                    }
                }
                if(promoItem.itemOneId !==0){
                    console.log(promoItem.itemOneId);
                    postItemTerm(itemTermOne).then((response) => {
                        console.log(promoItem.itemTwoId)
                        if(promoItem.itemTwoId !==0){
                            postItemTerm(itemTermTwo).then((response) => {
                                if(promoItem.itemThreeId !=="0"){
                                    postItemTerm(itemTermThree)
                                }
                            })
                        }
                    }).catch(() => {
                        Swal.fire({
                            icon: 'error',
                            text: 'Post Promo Discount Error!'
                        })
                    })
                }
            }).then(
                Swal.fire({
                    icon: 'success',
                    text: 'New Discount Promotion Posted'
                })
            ).catch(() => {
                Swal.fire({
                    icon: 'error',
                    text: 'Post Promo Discount Error!'
                })
            })
        }
        
    }

    function addPromoFreeGoods(events){
        events.preventDefault();
        let obj={
            "promo": {
                "user": {
                "user_id": localStorage.getItem("user_id")
                },
                "document_number": promoData.documentNumber,
                "description": promoData.description,
                "start_date": Moment(promoData.startDate).format('YYYY-MM-DD'),
                "end_date": Moment(promoData.endDate).format('YYYY-MM-DD'),
                "promo_value": promoData.promoValue
            },
            "quantity": promoType.freeGoodsQuantity,
            "product_code": promoType.freeGoodsCode
          }
        postPromoFreeGoods(obj).then((response) => {
            console.log(response.data.data.promotion_id)
            let itemTermOne={
                "product": {
                    "product_id": promoItem.itemOneId
                },
                "promo": {
                    "promotion_id": response.data.data.promotion_id
                }
            }
            let itemTermTwo={
                "product": {
                    "product_id": promoItem.itemTwoId
                },
                "promo": {
                    "promotion_id": response.data.data.promotion_id
                }
            }
            let itemTermThree={
                "product": {
                    "product_id": promoItem.itemThreeId
                },
                "promo": {
                    "promotion_id": response.data.data.promotion_id
                }
            }
            if(promoItem.itemOneId !==0){
                console.log(promoItem.itemOneId);
                postItemTerm(itemTermOne).then((response) => {
                    console.log(promoItem.itemTwoId)
                    if(promoItem.itemTwoId !==0){
                        postItemTerm(itemTermTwo).then((response) => {
                            if(promoItem.itemThreeId !=="0"){
                                postItemTerm(itemTermThree)
                            }
                        })
                    }
                }).catch(() => {
                    Swal.fire({
                        icon: 'error',
                        text: 'Error Posting Promotion !'
                    })
                })
            }
        }).then(
            Swal.fire({
                icon: 'success',
                text: 'New Free Goods Promotion Posted'
            })
        ).catch(() => {
            Swal.fire({
                icon: 'error',
                text: 'Post Promo Free Goods Error!'
            })
        })
    }

    return(
        <div className='mt-5'>
            <Row className="mt-2 mx-1">
                <Col>
                    <PromoData promoData={ promoData } setPromoData={ setPromoData }/>
                </Col>
                <Col>
                    <PromoItem promoItem={ promoItem } setPromoItem={ setPromoItem }/>
                    <PromoType promoType={ promoType } setPromoType={ setPromoType }
                    addPromoDiscount={ addPromoDiscount } addPromoFreeGoods={ addPromoFreeGoods }/>
                </Col>            
                
            </Row>
        </div>
    );
}

export default AddPromotion;