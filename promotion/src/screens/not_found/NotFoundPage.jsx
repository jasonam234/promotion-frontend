import './NotFoundPage.css'

const NotFoundPage =()=>{

    return(
        <div className="mt-5">
            <p className="not-found">404 Not Found</p>
            <p className="not-found-subtext">OptiPOS</p>
        </div>
    )
}
export default NotFoundPage