import { Link, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { authActions } from '../../store/auth';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Swal from 'sweetalert2';

import './NavBarComponent.css'

const NavBarComponent = () => {
    let navigate = useNavigate();
    let dispatch = useDispatch();

    function logoutHandler(){
        Swal.fire({
            title: 'Logout Now ?',
            confirmButtonText: 'Yes',
            confirmButtonColor: '#dc3741',
            showCancelButton: SVGComponentTransferFunctionElement,
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(authActions.logout());
                navigate("/");
            }
        })
    }

    return(
        <Navbar className="custom-navbar" bg="light" variant="light">
            <Container fluid>
                <Navbar.Brand>OptiPOS</Navbar.Brand>
                <Nav className="me-auto navbar-menu-font">
                    <Nav.Link as={Link} to="/promotion">Promotion</Nav.Link>
                    <Nav.Link as={Link} to="/transaction">Transaction</Nav.Link>
                </Nav>
            </Container>
            <Navbar.Collapse className="justify-content-end mx-3">
                {/* <Navbar.Text onClick={ () => logoutHandler }>
                    Logout
                </Navbar.Text> */}
                <Nav.Item>
                    <Nav.Link onClick={ () => logoutHandler() }>Logout</Nav.Link>
                </Nav.Item>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavBarComponent;