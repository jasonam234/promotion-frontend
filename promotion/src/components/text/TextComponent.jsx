import './TextComponent.css'

const TextComponent = (props) => {
    return(
        <p className = { props.className }>{props.content}</p>
    );
}

export default TextComponent;