import Form from 'react-bootstrap/Form';
import FormControlComponent from './FormControlComponent';

import './FormComponent.css'

const FormComponent = (props) => {
    return(
        <Form>
            <Form.Group className={ props.className } onChange={ props.onChange } controlId= { props.controlId }>
                <FormControlComponent type={ props.type } placeholder={ props.placeholder } as={ props.as } 
                    rows={ props.rows } readOnly={ props.readOnly } isRequired={ props.isRequired } 
                    value={ props.value } defaultValue={ props.defaultValue }/>
            </Form.Group>
        </Form>
    );
}

export default FormComponent;