import Form from 'react-bootstrap/Form';

const FormControlComponent = (props) => {
    if(props.readOnly === "yes"){
        return(
            <Form.Control type={ props.type } placeholder={ props.placeholder } as ={ props.as } rows={ props.rows } 
                value={ props.value } defaultValue={ props.defaultValue } readOnly/>
        );
    }else if(props.isRequired === "yes"){
        return(
            <Form.Control type={ props.type } placeholder={ props.placeholder } as ={ props.as } rows={ props.rows }
                value={ props.value } defaultValue={ props.defaultValue } required/>
        );
    }
    return(
        <Form.Control type={ props.type } placeholder={ props.placeholder } as ={ props.as } rows={ props.rows } 
            value={ props.value } defaultValue={ props.defaultValue }/>
    );
}

export default FormControlComponent;