import './ButtonComponent.css'

const ButtonComponent = (props) => {
    return(
        <button 
        className={ props.className } 
        onClick={ props.onClick } 
        disabled={ props.disabled }>{ props.content }</button>
    );
}

export default ButtonComponent;