import api from ".";

let token;

export async function getAllPromotion(){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/promos`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
}

export async function getAllDocument(){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/document_number`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
}

export async function getAllProductId(){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/product_id`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
}

export async function postPromoSearch(data){
  token = localStorage.getItem("token");
  let fetching = await api.post(`/promotion/post/search/promo`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  console.log(fetching);
  return fetching;
}

export async function getItemCodeSearch(product_code){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/product/${product_code}`,{
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function postPromoDiscount(data){
  token = localStorage.getItem("token");
  let fetching = await api.post(`/promotion/post/discount`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function postPromoFreeGoods(data){
  token = localStorage.getItem("token");
  let fetching = await api.post(`/promotion/post/free_goods`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}


export async function postItemTerm(data){
  token = localStorage.getItem("token");
  let fetching = await api.post(`/promotion/post/item_term`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function getPromotionByPromoId(promotion_id){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/promos/${promotion_id}`,{
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function getItemTermByPromoId(promotion_id){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/item_term/${promotion_id}`,{
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function getDiscountPromoByPromoId(promotion_id){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/discount/${promotion_id}`,{
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function getFreeGoodsPromoByPromoId(promotion_id){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/promotion/get/free_goods/${promotion_id}`,{
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function updateDiscountPromo(data){
  token = localStorage.getItem("token");
  let fetching = await api.put(`/promotion/put/discount`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function updateFreeGoodsPromo(data){
  token = localStorage.getItem("token");
  let fetching = await api.put(`/promotion/put/free_goods`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function updateItemTerm(data){
  token = localStorage.getItem("token");
  let fetching = await api.put(`/promotion/put/item_term`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function deletePromotion(promotion_id){
  token = localStorage.getItem("token");
  let fetching = await api.delete(`/promotion/delete/promo/${promotion_id}`,{
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}