import api from ".";

let token = localStorage.getItem("token");

export async function login(form) {
  let fetching = await api.post(`/authenticate`, form);
  console.log(fetching);
  return fetching;
}

export const register = async (form) => {
  let fetching = await api.post(`/register`, form);
  return fetching;
};