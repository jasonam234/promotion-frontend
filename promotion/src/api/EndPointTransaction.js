import api from ".";

let token;

export async function getAllTransaction(){
  token = localStorage.getItem("token");
  let fetching = await api.get(`/transaction/get/transactions`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
}

export async function postTransactionSearch(data){
  token = localStorage.getItem("token");
  let fetching = await api.post(`/transaction/post/search/transaction`, data, {
    headers: {Authorization: `Bearer ${token}`},
  });
  return fetching;
}

export async function generateInvoicePdf(transaction_id){
  token = localStorage.getItem("token");
  let fetching = await api.post(`/transaction/post/invoice/${transaction_id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
}
